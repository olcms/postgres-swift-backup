# Simple backups for postgresql

A docker image that will do backups of a list of postgres
databases.

Configuration is done via enviornment variables.

``POSTGRESQL_BACKUP_DATABASES_TO_BACKUP``
    Semicolon separated list of databases

``POSTGRESQL_BACKUP_SWIFT_*``
    Swift login parameters.

``POSTGRESQL_BACKUP_PG*`` and others
    Postgres login (for all databases)

``SMTP_*``
    Emails that will be notified of errors
