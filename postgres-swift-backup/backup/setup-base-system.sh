#!/usr/bin/env bash

set -e

apt-get update
apt-get upgrade -yq

# For minor developement tasks inside the container. Never regretted installing basic tools
apt-get install telnet vim nano less grep -yq

apt-get install python-pip -yq

pip install -r /backup/requirements.txt

apt-get autoremove -y
apt-get clean
rm -rf /var/cache/*
rm -rf /var/tmp/*
rm -rf /tmp/*

rm -rf /root/.npm
rm -rf /root/.cache


