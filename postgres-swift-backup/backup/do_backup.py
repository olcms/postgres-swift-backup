#!/usr/bin/env python
import datetime
import sys
import os
import shutil
import tempfile
import simplemail
import time


class BackupManager(object):

    def __init__(self, database):
        self.tempdir = tempfile.mkdtemp()
        self.database = database
        self.timestamp = datetime.datetime.now()
        self.output_file = os.path.join(self.tempdir, "{}-{}.sql.xz".format(database, self.timestamp.isoformat()))

    def cleanup(self):
        shutil.rmtree(str(self.tempdir))

    def system(self, command):
        response = os.system(command)
        print(response, command)
        if response != 0:
            raise ValueError(command)

    def backup_database(self):

        command_template = (
            'bash -c "'
            'set -e && set -o pipefail && '
            'cd {} && '
            'pg_dump {} | xz > {}"'
        )

        self.system(command_template.format(
            self.tempdir,
            self.database,
            self.output_file
        ))

    def send_to_swift(self):
        self.system(
            'bash -c "' +
            "cd {} && ".format(self.tempdir) +
            "swift -A ${POSTGRESQL_BACKUP_SWIFT_AUTH_URL} " +
            "-U ${POSTGRESQL_BACKUP_SWIFT_USER} " +
            "-K ${POSTGRESQL_BACKUP_SWIFT_PASSWORD} " +
            "upload " +
            "${POSTGRESQL_BACKUP_SWIFT_CONTAINER} " +
            os.path.basename(self.output_file) +
            '"'
        )

    def do_backup(self):
        try:
            self.backup_database()
            self.send_to_swift()
        finally:
            self.cleanup()

    def send_error_emails(self, msgs):

        message = ["Some backups failed. List is here"]
        for k, v in msgs.items():
            message.append(" * **{}**: {}".format(k, v))
        for recipient in os.environ['SMTP_TO_LIST'].split(';'):
            if not recipient.strip():
                continue
            simplemail.Email(
                smtp_server=os.environ['SMTP_HOST'],
                smtp_user=os.environ['SMTP_USER'],
                smtp_password=os.environ['SMTP_PASSWORD'],
                from_address=os.environ['SMTP_FROM'],
                use_ssl=os.environ['SMTP_USE_SSL'] == "true",
                to_address=recipient,
                subject=u"Errors in backup",
                message="\n".join(message)
            ).send()


def backup_databases(databases):
    errors = {}
    for database in databases:
        if not database.strip():
            continue
        try:
            backup = BackupManager(database)
            backup.do_backup()
        except Exception as e:
            errors[database] = e.args
        if errors:
            BackupManager("").send_error_emails(errors)


def update_enviornment():
    prefix = "POSTGRESQL_BACKUP_"
    for k, v in os.environ.items():
        if k.startswith("POSTGRESQL_BACKUP_PG"):
            os.environ[k[len(prefix):]] = v

if __name__ == "__main__":

    update_enviornment()

    if len(sys.argv) > 1 and sys.argv[1] == "test-emails":
        BackupManager("test").send_error_emails({"test": "Not a real problem just testing!"})
        sys.exit(1)

    databases_to_backup = os.environ['POSTGRESQL_BACKUP_DATABASES_TO_BACKUP']

    while True:
        backup_databases(databases_to_backup.split(";"))
        print("Did backups! Done at: " + datetime.datetime.now().isoformat())
        time.sleep(6*3600)
